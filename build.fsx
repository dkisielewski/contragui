#r @"packages/FAKE/tools/FakeLib.dll"
open Fake
open Fake.AssemblyInfoFile

let buildDir = "./build/"
let deployDir = "./deploy/"
let version = "0.2"

RestorePackages()

Target "Clean" (fun _ ->
    CleanDirs [buildDir; deployDir]
)

Target "SetAssemblyInfo" (fun _ ->
    CreateCSharpAssemblyInfo "./src/ContraGUI.Base/Properties/AssemblyInfo.cs"
        [Attribute.Title "ContraGUI Base Library"
         Attribute.Product "ContraGUI.Base"
         Attribute.Copyright "Copyright © Jakub Fijałkowski 2015"
         Attribute.Guid "a65c298f-044c-49ba-a08a-d42a3385066d"
         Attribute.Version version
         Attribute.FileVersion version]

    CreateCSharpAssemblyInfo "./src/ContraGUI.Sample/Properties/AssemblyInfo.cs"
        [Attribute.Title "ContraGUI Sample Application"
         Attribute.Product "ContraGUI.Sample"
         Attribute.Copyright "Copyright © Jakub Fijałkowski 2015"
         Attribute.Guid "e1b8c8bf-e42d-446d-9a4d-c3c4cd2aebbd"
         Attribute.Version version
         Attribute.FileVersion version]
)

Target "BuildLib" (fun _ ->
    !! "src/ContraGUI.Base/ContraGUI.Base.csproj"
      |> MSBuildReleaseExt buildDir [ ("GenerateDocumentation", "true"); ("DocumentationFile", buildDir @@ "ContraGUI.Base.xml") ] "Build"
      |> Log "AppBuild-Output: "
)

Target "CreatePackage" (fun _ ->
    NuGet (fun p ->
        { p with
            Version = version
            OutputPath = deployDir
            WorkingDir = buildDir
            Publish = false }) "ContraGUI.Base.nuspec"
)

Target "CompileSample" (fun _ ->
    !! "src/ContraGUI.Sample/ContraGUI.Sample.csproj"
      |> MSBuildRelease buildDir "Build"
      |> Log "AppBuild-Output: "
)

Target "RunSample" (fun _ ->
    let result =
        ExecProcess (fun info -> 
            info.FileName <- buildDir + "ContraGUI.Sample.exe"
        ) (System.TimeSpan.FromMinutes 1.)

    if result <> 0 then failwith "Failed to run sample application"
)

Target "Default" DoNothing

"Clean"
  ==> "SetAssemblyInfo"
  ==> "BuildLib"
  ==> "CreatePackage"
  ==> "Default"

"Clean"
  ==> "SetAssemblyInfo"
  ==> "CompileSample"
  ==> "RunSample"

RunTargetOrDefault "Default"
