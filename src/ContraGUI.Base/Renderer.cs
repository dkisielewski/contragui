﻿using System;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Direct3D10;
using SharpDX.DXGI;

namespace ContraGUI.Base
{
    using Device1 = SharpDX.Direct3D10.Device1;
    using Factory = SharpDX.Direct2D1.Factory;

    class Renderer
        : IRenderer, IDisposable
    {
        private readonly Device1 device;
        private readonly SwapChain swapChain;
        private readonly Factory factory;

        private readonly ObjectList objects;
        private readonly GraphicsFactory graphicsFactory;

        private Texture2D backBuffer;
        private RenderTargetView renderView;
        private Surface surface;
        private RenderTarget renderTarget;

        public IObjectList Objects
        {
            get { return this.objects; }
        }

        public IGraphicsFactory Factory
        {
            get { return this.graphicsFactory; }
        }

        public Renderer(Device1 device, SwapChain swapChain)
        {
            this.device = device;
            this.swapChain = swapChain;
            this.factory = new Factory();

            this.objects = new ObjectList();
            this.graphicsFactory = new GraphicsFactory(this.factory);
        }

        public void AcquireRenderTarget()
        {
            if (this.renderTarget != null)
            {
                return;
            }
            this.backBuffer = Texture2D.FromSwapChain<Texture2D>(this.swapChain, 0);
            this.renderView = new RenderTargetView(this.device, this.backBuffer);
            this.surface = backBuffer.QueryInterface<Surface>();
            this.renderTarget = new RenderTarget(this.factory, this.surface, new RenderTargetProperties(new PixelFormat(Format.Unknown, AlphaMode.Premultiplied)));
            this.graphicsFactory.RenderTarget = this.renderTarget;
        }

        public void ReleaseRenderTarget()
        {
            if (this.renderTarget == null)
            {
                return;
            }

            this.renderTarget.Dispose();
            this.surface.Dispose();
            this.renderView.Dispose();
            this.backBuffer.Dispose();

            this.renderTarget = null;
            this.surface = null;
            this.renderView = null;
            this.backBuffer = null;
            this.graphicsFactory.RenderTarget = null;
        }

        public void Dispose()
        {
            this.objects.Dispose();

            this.ReleaseRenderTarget();
            this.graphicsFactory.Dispose();
            this.factory.Dispose();
        }

        public void Render()
        {
            this.renderTarget.BeginDraw();
            this.renderTarget.Clear(Color.White);

            foreach (var obj in this.objects)
            {
                obj.Render(this.renderTarget);
            }

            this.renderTarget.EndDraw();

            swapChain.Present(0, PresentFlags.None);
        }
    }
}
