﻿using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DirectWrite;

namespace ContraGUI.Base
{
    /// <summary>
    /// Represents a text that will be drawn by the renderer.
    /// </summary>
    public sealed class TextObject
        : RendererObject
    {
        /// <summary>
        /// Gets or sets the position of the text.
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// Gets the format of the text.
        /// </summary>
        public TextFormat Format { get; private set; }

        /// <summary>
        /// Gets the layout of the text.
        /// </summary>
        public TextLayout Layout { get; private set; }

        internal TextObject(Vector2 position, TextFormat format, TextLayout layout, Brush brush)
        {
            this.Position = position;
            this.Format = format;
            this.Layout = layout;
            this.Brush = brush;
        }

        /// <inheritdoc />
        public override void Dispose(bool disposeBrush)
        {
            this.Format.Dispose();
            this.Layout.Dispose();
            if (disposeBrush)
            {
                this.Brush.Dispose();
            }
        }

        internal override void Render(RenderTarget renderTarget)
        {
            renderTarget.DrawTextLayout(this.Position, this.Layout, this.Brush);
        }
    }
}
