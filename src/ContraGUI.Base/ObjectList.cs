﻿using System;
using System.Collections.Generic;

namespace ContraGUI.Base
{
    sealed class ObjectList
        : IObjectList
    {
        private readonly List<RendererObject> list = new List<RendererObject>();

        public int Count
        {
            get { return this.list.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public RendererObject this[int index]
        {
            get { return this.list[index]; }
            set { this.list[index] = value; }
        }

        public void Add(RendererObject item)
        {
            this.list.Add(item);
        }

        public void Insert(int index, RendererObject item)
        {
            this.list.Insert(index, item);
        }

        public void Clear(bool dispose, bool disposeBrushes)
        {
            if (dispose)
            {
                foreach (var item in this.list)
                {
                    item.Dispose(disposeBrushes);
                }
            }
            this.list.Clear();
        }

        public void Clear()
        {
            this.Clear(true, true);
        }

        public void Dispose()
        {
            this.Clear(true, true);
        }

        public bool Remove(RendererObject item, bool dispose, bool disposeBrush)
        {
            if (this.list.Remove(item))
            {
                if (dispose)
                {
                    item.Dispose(disposeBrush);
                }
                return true;
            }
            return false;
        }

        public bool Remove(RendererObject item)
        {
            return this.Remove(item, true, true);
        }

        public void RemoveAt(int index, bool dispose, bool disposeBrush)
        {
            if (index < 0 || index >= this.Count)
            {
                throw new ArgumentOutOfRangeException("index");
            }
            if (dispose)
            {
                this.list[index].Dispose(disposeBrush);
            }
            this.list.RemoveAt(index);
        }

        public void RemoveAt(int index)
        {
            this.RemoveAt(index, true, true);
        }

        public int IndexOf(RendererObject item)
        {
            return this.list.IndexOf(item);
        }

        public bool Contains(RendererObject item)
        {
            return this.list.Contains(item);
        }

        public void CopyTo(RendererObject[] array, int arrayIndex)
        {
            this.list.CopyTo(array, arrayIndex);
        }

        public IEnumerator<RendererObject> GetEnumerator()
        {
            return this.list.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.list.GetEnumerator();
        }
    }
}
