﻿using SharpDX.Direct2D1;

namespace ContraGUI.Base
{
    /// <summary>
    /// Represents a primitive object that is being drawn as stroke only.
    /// </summary>
    public sealed class StrokedObject
        : RendererObject
    {
        /// <summary>
        /// Gets a geometry of the object.
        /// </summary>
        /// <remarks>
        /// If you need to manipulate the geometry, feel free to cast it to concrete type.
        /// </remarks>
        public Geometry Geometry { get; private set; }

        /// <summary>
        /// Gets or sets the width of stroke.
        /// </summary>
        public float StrokeWidth { get; set; }

        internal StrokedObject(Geometry geometry, Brush brush, float strokeWidth)
        {
            this.Geometry = geometry;
            this.Brush = brush;
            this.StrokeWidth = strokeWidth;
        }

        /// <inheritdoc />
        public override void Dispose(bool disposeBrush)
        {
            this.Geometry.Dispose();
            if (disposeBrush)
            {
                this.Brush.Dispose();
            }
        }

        internal override void Render(RenderTarget renderTarget)
        {
            renderTarget.DrawGeometry(this.Geometry, this.Brush, this.StrokeWidth);
        }
    }
}
