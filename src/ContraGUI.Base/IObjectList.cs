﻿using System;
using System.Collections.Generic;

namespace ContraGUI.Base
{
    /// <summary>
    /// Represents list of <see cref="RendererObjects"/> and the order in which they are rendered.
    /// </summary>
    /// <remarks>
    /// Indexer DOES NOT dispose replaced object.
    ///
    /// All methods that remove objects and do not allow to specify whether to dispose the object
    /// or not, dispose them.
    /// </remarks>
    /// TODO: dispose objects in Clear/Remove?
    public interface IObjectList
        : IList<RendererObject>, IDisposable
    {
        /// <summary>
        /// Clears the list of objects. Allows to decide whether to dispose objects or not.
        /// </summary>
        /// <param name="dispose"></param>
        /// <param name="disposeBrushes"></param>
        void Clear(bool dispose, bool disposeBrushes);

        /// <summary>
        /// Removes specified item. Allows to decide whether to dispose the object or not.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="dispose"></param>
        /// <param name="disposeBrush"></param>
        /// <returns></returns>
        bool Remove(RendererObject item, bool dispose, bool disposeBrush);

        /// <summary>
        /// Removes specified item. Allows to decide whether to dispose the object or not.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="dispose"></param>
        /// <param name="disposeBrush"></param>
        void RemoveAt(int index, bool dispose, bool disposeBrush);
    }
}
