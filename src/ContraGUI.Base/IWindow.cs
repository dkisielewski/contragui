﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ContraGUI.Base
{
    /// <summary>
    /// A window. Provides access to events and a title of underlying window
    /// </summary>
    /// <remarks>
    /// See <see cref="System.Windows.Forms.Form"/> and <see cref="System.Windows.Forms.Control"/> for more
    /// info about these events.
    /// </remarks>
    /// <seealso cref="System.Windows.Forms.Control"/>
    /// <seealso cref="System.Windows.Forms.Form"/>
    public interface IWindow
    {
#pragma warning disable 1591
        string Text { get; set; }
        Size ClientSize { get; }

        event EventHandler Load;
        event EventHandler<EventArgs> UserResized;

        event EventHandler Activated;
        event EventHandler Deactivate;

        event CancelEventHandler Closing;
        event EventHandler Closed;

        event EventHandler MouseEnter;
        event MouseEventHandler MouseMove;
        event EventHandler MouseLeave;

        event MouseEventHandler MouseDown;
        event MouseEventHandler MouseUp;
        event MouseEventHandler MouseWheel;

        event KeyEventHandler KeyDown;
        event KeyEventHandler KeyUp;
#pragma warning restore 1591
    }
}
