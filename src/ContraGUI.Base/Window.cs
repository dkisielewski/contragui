﻿using SharpDX.Windows;

namespace ContraGUI.Base
{
    class Window
        : RenderForm, IWindow
    {
        public Window()
        {
            this.Icon = null;
        }
    }
}
