﻿using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DirectWrite;

namespace ContraGUI.Base
{
    /// <summary>
    /// Factory of graphic objects.
    /// </summary>
    /// <remarks>
    /// This class, nor any related one, does not manage the lifecycle of the objects - you
    /// should dispose unused ones.
    ///
    /// "Fill" means that the geometry will be drawn filled, "draw" means that only stroke
    /// will be drawn and the whole figure won't be filled.
    /// </remarks>
    public interface IGraphicsFactory
    {
#pragma warning disable 1591
        SolidColorBrush CreateSolidColorBrush(Color4 color);
        LinearGradientBrush CreateLinearGradientBrush(LinearGradientBrushProperties properties, params GradientStop[] gradientStops);
        RadialGradientBrush CreateRadialGradientBrush(RadialGradientBrushProperties properties, params GradientStop[] gradientStops);

        FilledObject FillRectangle(RectangleF rect, Brush brush);
        FilledObject FillRoundedRectangle(RectangleF rect, float radiusX, float radiusY, Brush brush);
        FilledObject FillEllipse(Vector2 center, float radiusX, float radiusY, Brush brush);

        StrokedObject DrawRectangle(RectangleF rect, float strokeWidth, Brush brush);
        StrokedObject DrawRoundedRectangle(RectangleF rect, float radiusX, float radiusY, float strokeWidth, Brush brush);
        StrokedObject DrawEllipse(Vector2 center, float radiusX, float radiusY, float strokeWidth, Brush brush);
        StrokedObject DrawLine(Vector2 start, Vector2 end, float width, Brush brush);

        TextObject DrawText(Vector2 position, string text, string fontName, FontWeight fontWeight, FontStyle fontStyle, float fontSize, float maxWidth, float maxHeight, Brush brush);
#pragma warning restore 1591

        /// <summary>
        /// Creates a <see cref="FilledObject"/> with empty <see cref="PathGeometry"/> that will be drawn filled.
        /// See https://msdn.microsoft.com/en-us/library/windows/desktop/ee264309(v=vs.85).aspx for
        /// information on how to fill <see cref="PathGeometry"/>.
        /// </summary>
        /// <param name="brush"></param>
        /// <returns></returns>
        FilledObject FillPath(Brush brush);

        /// <summary>
        /// Creates a <see cref="StrokedObject"/> with empty <see cref="PathGeometry"/> that will be drawn stroked.
        /// See https://msdn.microsoft.com/en-us/library/windows/desktop/ee264309(v=vs.85).aspx for
        /// information on how to fill <see cref="PathGeometry"/>.
        /// </summary>
        /// <param name="brush"></param>
        /// <returns></returns>
        StrokedObject DrawPath(float strokeWidth, Brush brush);
    }
}
