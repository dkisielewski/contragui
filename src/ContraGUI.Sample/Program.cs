﻿using System;
using System.Collections.Generic;
using ContraGUI.Base;
using SharpDX;
using SharpDX.Direct2D1;

namespace ContraGUI.Sample
{
    using Point = System.Drawing.Point;

    class Program
        : Application
    {
        private const float RectSize = 40;
        private const float StrokeWidth = 2;
        private const float StrokedRectSize = RectSize - StrokeWidth;

        private readonly List<Func<Point, Brush>> availableBrushes;

        private SolidColorBrush textBrush;
        private SolidColorBrush lineBrush;

        private RendererObject mouseLine;
        private RendererObject brushSample;
        private TextObject title;

        private int selectedBrush = 0;

        static void Main(string[] args)
        {
            new Program().Run();
        }

        public Program()
        {
            this.availableBrushes = new List<Func<Point, Brush>>
            {
                this.CreateBrush1,
                this.CreateBrush2,
                this.CreateBrush3
            };
        }

        protected override void OnInitialize()
        {
            this.MainWindow.Text = "Sample application";

            this.MainWindow.MouseMove += this.OnMouseMove;
            this.MainWindow.MouseDown += this.OnMouseDown;
            this.MainWindow.MouseWheel += this.OnMouseWheel;

            this.textBrush = this.Renderer.Factory.CreateSolidColorBrush(Color.Red);
            this.lineBrush = this.Renderer.Factory.CreateSolidColorBrush(Color.Khaki);

            float halfWidth = this.MainWindow.ClientSize.Width / 2.0f;
            float halfHeight = this.MainWindow.ClientSize.Height / 2.0f;

            this.title = this.Renderer.Factory.DrawText(
                position: new Vector2(0, 0),
                text: "Sample app",
                fontName: "Comic Sans MS",
                fontWeight: SharpDX.DirectWrite.FontWeight.Regular,
                fontStyle: SharpDX.DirectWrite.FontStyle.Normal,
                fontSize: 20.0f,
                maxWidth: this.MainWindow.ClientSize.Width,
                maxHeight: this.MainWindow.ClientSize.Height,
                brush: textBrush
            );
            float textWidth = this.title.Layout.DetermineMinWidth();
            this.title.Position = new Vector2(halfWidth - textWidth / 2.0f, 5);
            this.Renderer.Objects.Add(this.title);

            var brush = this.availableBrushes[0](new Point((int)halfWidth, (int)halfHeight));
            this.brushSample = this.Renderer.Factory.FillRoundedRectangle(
                new RectangleF(halfWidth - RectSize / 2, halfHeight - RectSize / 2, RectSize, RectSize),
                StrokeWidth, StrokeWidth, brush);
            this.Renderer.Objects.Add(this.brushSample);
        }

        private void OnMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (this.mouseLine != null)
            {
                this.Renderer.Objects.Remove(this.mouseLine, true, false);
            }

            float halfWidth = this.MainWindow.ClientSize.Width / 2.0f;
            float halfHeight = this.MainWindow.ClientSize.Height / 2.0f;

            this.mouseLine = this.Renderer.Factory.DrawLine(
                new Vector2(halfWidth, halfHeight),
                new Vector2(e.X, e.Y),
                2, this.lineBrush);
            this.Renderer.Objects.Add(this.mouseLine);
        }

        private void OnMouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            int halfWidth = this.MainWindow.ClientSize.Width / 2;
            int halfHeight = this.MainWindow.ClientSize.Height / 2;

            this.selectedBrush = (this.selectedBrush + 1) % this.availableBrushes.Count;
            this.brushSample.Brush.Dispose();
            this.brushSample.Brush = this.availableBrushes[this.selectedBrush](new Point(halfWidth, halfHeight));
        }

        private void OnMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Unfortunately, brush space(coordinates) is not relative to the object, but rather is the current coordinate system of the render target(ie. window).
            var pointBrush = this.availableBrushes[this.selectedBrush](e.Location);
            RendererObject obj = null;
            switch (e.Button)
            {
                case System.Windows.Forms.MouseButtons.Left:
                    obj = this.Renderer.Factory.FillRectangle(new RectangleF(e.X - RectSize / 2, e.Y - RectSize / 2, RectSize, RectSize), pointBrush);
                    break;
                case System.Windows.Forms.MouseButtons.Middle:
                    obj = this.Renderer.Factory.FillEllipse(new Vector2(e.X, e.Y), RectSize / 2, RectSize / 2, pointBrush);
                    break;
                case System.Windows.Forms.MouseButtons.Right:
                    obj = this.Renderer.Factory.DrawRectangle(new RectangleF(e.X - StrokedRectSize / 2, e.Y - StrokedRectSize / 2, StrokedRectSize, StrokedRectSize), StrokeWidth, pointBrush);
                    break;
            }
            this.Renderer.Objects.Add(obj);
        }

        private Brush CreateBrush1(Point location)
        {
            return this.Renderer.Factory.CreateLinearGradientBrush(
                new LinearGradientBrushProperties
                {
                    StartPoint = new Vector2(location.X - RectSize / 2, location.Y - RectSize / 2),
                    EndPoint = new Vector2(location.X + RectSize / 2, location.Y + RectSize / 2)
                },
                new GradientStop { Color = Color.Red, Position = 0 },
                new GradientStop { Color = Color.Green, Position = 1 });
        }

        private Brush CreateBrush2(Point location)
        {
            return this.Renderer.Factory.CreateRadialGradientBrush(
                new RadialGradientBrushProperties
                {
                    Center = new Vector2(location.X, location.Y),
                    GradientOriginOffset = Vector2.Zero,
                    RadiusX = RectSize / 2,
                    RadiusY = RectSize / 2
                },
                new GradientStop { Color = Color.HotPink, Position = 0 },
                new GradientStop { Color = Color.Khaki, Position = 1 });
        }

        private Brush CreateBrush3(Point location)
        {
            return this.Renderer.Factory.CreateSolidColorBrush(Color.DarkMagenta);
        }
    }
}
